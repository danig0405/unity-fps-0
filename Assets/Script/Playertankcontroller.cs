﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playertankcontroller : MonoBehaviour
{
    public float speed = 4f;
    public float angularspeed = 30f;                               

    // Update is called once per frame
    void Update()
    {
        // Rotation
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.up * (-angularspeed * Time.deltaTime));
        }
        else if (Input.GetKey(KeyCode.D)) 
        {
            transform.Rotate(Vector3.up * (angularspeed * Time.deltaTime));
        }

        // Movement
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * (Time.deltaTime * speed));
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * (Time.deltaTime * speed));
        }
    }

}
